<?php
/**
 * Clase para consumir API Rest
 * Las operaciones soportadas son:.
 *
 *	- POST		: Agregar
 *	- GET		: Consultar
 *	- DELETE	: Eliminar
 *	- PUT		: Actualizar
 *	- PATCH		: Actualizar por parte
 *
 * Extras
 *	- autenticación de acceso básica (Basic Auth)
 *	- Conversor JSON
 *
 * @author		Diego Valladares <dvdeveloper.com>
 *
 * @version		1.0
 */
class API
{
	/**
	 * autenticación de acceso básica (Basic Auth)
	 * Ejemplo Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==.
	 *
	 * @param string $URL      url para acceder y obtener un token
	 * @param string $usuario  usuario
	 * @param string $password clave
	 *
	 * @return JSON
	 */
	private static $tokenUrl = 'https://wp1.cirel.com.ve/wp-json/jwt-auth/v1/token';
	private static $apiUrl = 'https://wp1.cirel.com.ve/wp-json/wc/v2/';
	private static $param = array(
		'username' => 'cirelramos',
		'password' => 'Contenido-12345678',
	);
	private static $token;

	public function __construct()
	{
		self::Authentication();
	}

	public static function Authentication()
	{
		$URL = self::$tokenUrl;
		$ARRAY = self::$param;
		$datapost = '';
		foreach ($ARRAY as $key => $value) {
			$datapost .= $key.'='.$value.'&';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$result = curl_exec($ch);
		curl_close($ch);
		$result = self::JSON_TO_ARRAY($result);
		$result = $result['token'];
		self::$token = $result;

		return $result;
	}

	/**
	 * Enviar parámetros a un servidor a través del protocolo HTTP (POST).
	 * Se utiliza para agregar datos en una API REST.
	 *
	 * @param string $URL URL recurso, ejemplo: http://website.com/recurso
	 * @param string self::$token token de autenticación
	 * @param array $ARRAY parámetros a envíar
	 *
	 * @return JSON
	 */
	public static function POST($URL, $ARRAY)
	{
		$datapost = '';
		$datapost = http_build_query($ARRAY);
		$headers = array('Authorization: Bearer '.self::$token);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$apiUrl.''.$URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Consultar a un servidor a través del protocolo HTTP (GET).
	 * Se utiliza para consultar recursos en una API REST.
	 *
	 * @param string $URL URL recurso, ejemplo: http://website.com/recurso/(id) no obligatorio
	 * @param string self::$token token de autenticación
	 *
	 * @return JSON
	 */
	public static function GET($URL)
	{
		$headers = array('Authorization: Bearer '.self::$token);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$apiUrl.''.$URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Consultar a un servidor a través del protocolo HTTP (DELETE).
	 * Se utiliza para eliminar recursos en una API REST.
	 *
	 * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
	 * @param string self::$token token de autenticación
	 *
	 * @return JSON
	 */
	public static function DELETE($URL)
	{
		$headers = array('Authorization: Bearer '.self::$token);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, self::$apiUrl.''.$URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Enviar parámetros a un servidor a través del protocolo HTTP (PUT).
	 * Se utiliza para editar un recurso en una API REST.
	 *
	 * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
	 * @param string self::$token token de autenticación
	 * @param array $ARRAY parámetros a envíar
	 *
	 * @return JSON
	 */
	public static function PUT($URL, $ARRAY)
	{
		$datapost = '';
		$datapost = http_build_query($ARRAY);
		$headers = array('Authorization: Bearer '.self::$token);
		$ch = curl_init();
		echo self::$apiUrl.''.$URL;
		curl_setopt($ch, CURLOPT_URL, self::$apiUrl.''.$URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Enviar parámetros a un servidor a través del protocolo HTTP (PATCH).
	 * Se utiliza para editar un atributo específico (recurso) en una API REST.
	 *
	 * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
	 * @param string self::$token token de autenticación
	 * @param array $ARRAY parametros parámetros a envíar
	 *
	 * @return JSON
	 */
	public static function PATCH($URL, $ARRAY)
	{
		$datapost = '';
		$datapost = http_build_query($ARRAY);
		$headers = array('Authorization: Bearer '.self::$token);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$apiUrl.''.$URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Convertir JSON a un ARRAY.
	 *
	 * @param json $json Formato JSON
	 *
	 * @return array
	 */
	public static function JSON_TO_ARRAY($json)
	{
		return json_decode($json, true);
	}

	public function __destruct()
	{
		self::$token = '';
	}
}
