<?php

if (isset($_POST['type'])) {
	require_once '../models/API.php';
	$apiObj = new API();
	switch ($_POST['type']) {
	case 'consultAll':
		$rs = $apiObj->GET("{$_POST['route']}");
		$array = $apiObj->JSON_TO_ARRAY($rs);
		echo $rs;
		return $rs;
		break;
	case 'insert':
		$rs = $apiObj->POST("{$_POST['route']}", $_POST['data']);
		return $rs;
		break;
	case 'update':
		$rs = $apiObj->PUT("{$_POST['route']}", $_POST['data']);
		echo $rs;
			return $rs;
		break;
	case 'delete':
		$rs = $apiObj->DELETE("{$_POST['route']}");

		return $rs;
		break;
	default:
		return 'internal problems';
		break;
	}
}
